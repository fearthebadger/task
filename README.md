# Task

This is a task tracking project written in `bash`. There are so many other tools out there that I felt were overly complicated and prehibative of some of the things that I wanted to do. So I wrote this a very simple script you can put in your `$PATH`.

With this script you have full control over where you want to store the data. Allowing for you to choose how you maintain this data.

- [Example Output](examples/README.md)

## Usage

``` text
Usage: task [OPTION]

  help || -h                Print this Usage statement.
  all  || -a                Print all the tasks recorded.

  [empty]                   Print all Todo and Backlogged tasks.
  [Task#]                   Print all relevant information about the given Task #.
  add "Task text"           Create a new task with the Task text provided.
  sub Task# 'Task text'     This will create a sub task to the \`task#\` provided.
  back                      Print out all tasks in backlog.
  back Task#                Move given Task # into backlog.
  done                      Print out all completed tasks.
  done Task#                Move given Task # to completed.
  edit Task#                Open the tasks.md file at the specified task line for editing.
  find [Text]               Search through all of the tasks to find ones that match the Text (case insensitive).
  nix                       Print out all nixed tasks.
  nix Task#                 Nix given Task # that you no longer want to work on.
  note(s)                   This will open a file with the name general.md
  note(s) Task#             Open a markdown note for the Task # given with your $EDITOR of choice.
  pin                       Pin a task to a pinned section for things that are closer to a perpetual note.
  todo                      Print out all tasks in todo.
  todo Task#                Move given Task # into todo.
```

### Expanded Usage

- To create multiple task lists based on environment.

    ``` bash
    alias wtask='TASK_DIR=${HOME}/work_tasks task'
    alias ptask='TASK_DIR=${HOME}/personal_tasks task'
    ```

- To Setup a simple push of the tasks to git you can add this to your bashrc

    ``` bash
    function pt() {
      (
        cd $TASK_DIR

        TODAY=`date`

        git add -A .
        git cm -m "Update $TODAY"
        git push
      )
    }
    ```

    - Simply type the command `pt` to push your tasks

## Optional environment variables

| Variable | Default Value | Description |
| - | - | - |
| `$EDITOR` | System Default | When editing any part of a task it will use this to open the file |
| `$TASK_DIR` | (default ⇒ ${HOME}/tasks/) | Set this if you want to change your tasks directory to something other than the default |

## Impacted files

| Filename | Description |
| - | - |
| `$TASK_DIR`/tasks.md | The main file which stores every task written. |
| `$TASK_DIR`/.tasks_data.md | Right now this has a utime stamp from the last action (add, back, done, nix, todo), for future potential. |
| `$TASK_DIR`/notes/general.md | The file created if you run `task note` |
| `$TASK_DIR`/notes/`$TASK_ID_NUMBER`.md | The file created when you create a note for a task. (ex. `task note 1` would create `$TASK_DIR/notes/1.md`) |

### Folder structure

``` console
$ tree -a tasks
tasks/
├── notes
│   ├── 17.md
│   ├── 1.md
│   ├── 31.md
│   └── general.md
├── .task_data.md
└── tasks.md
```
