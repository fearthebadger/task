# Example Output

- Empty Task List

    ![Empty Task List](no_tasks.jpg)

- Adding a new task

    ![First Task](first.jpg)

- Adding a sub-task

    ![Sub Task](sub.jpg)

- Move first task to `TODO`

    ![Todo Task](todo.jpg)

- Print the task list

    ![Second Task](tasks.jpg)

- Nix a task

    ![Nix Task](nix.jpg)
- Complete a task

    ![Done Task](done.jpg)

- The various color output
    ![Colors](color.jpg)
    ![Colors 2](color2.jpg)
